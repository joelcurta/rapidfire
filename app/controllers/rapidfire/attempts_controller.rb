module Rapidfire
  class AttemptsController < Rapidfire::ApplicationController
    before_filter :find_survey!

    def new
      @attempt_builder = AttemptBuilder.new(attempt_params)
      respond_to do |format|
        format.html
        format.js
      end
    end

    def create
      @attempt_builder = AttemptBuilder.new(attempt_params)

      if @attempt_builder.save
         respond_to do |format|
          format.html { redirect_to surveys_path }
          format.js
        end
      else
        render :new
      end
    end

    private
    def find_survey!
      @survey = Survey.find(params[:survey_id])
    end

    def attempt_params
      answer_params = { params: (params[:attempt] || {}) }
      answer_params.merge(user: current_user, survey: @survey)
    end
  end
end
