module Rapidfire
  class Attempt < ActiveRecord::Base
    belongs_to :survey
    belongs_to :user, polymorphic: true
    has_many   :answers, inverse_of: :attempt, autosave: true
    

    scope :for_survey, ->(survey) { where(:survey_id => survey.id) }
  	scope :exclude_survey,  ->(survey) { where("NOT survey_id = #{survey.id}") }
	  scope :for_participant, ->(participant) {
    	where(:participant_id => participant.try(:id), :participant_type => participant.class.base_class)
  	}


    if Rails::VERSION::MAJOR == 3
      attr_accessible :survey, :user
    end
  end
end
