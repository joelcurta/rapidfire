require 'active_model_serializers'

module Rapidfire
  class Engine < ::Rails::Engine
    isolate_namespace Rapidfire

    ActiveSupport.on_load :active_record do
      require 'rapidfire/active_record_extension'
      ::ActiveRecord::Base.send :include, Rapidfire::ActiveRecordExtension
    end
  end
end
