class AddColumnActiveToSurveys < ActiveRecord::Migration[5.0]
  def change
  	add_column :rapidfire_surveys, :active, :boolean, default: false
  end
end
