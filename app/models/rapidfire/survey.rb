module Rapidfire
  class Survey < ActiveRecord::Base
    has_many  :questions
    validates :name, :presence => true

    scope :active,   -> { where(:active => true) }
  	scope :inactive, -> { where(:active => false) }

    if Rails::VERSION::MAJOR == 3
      attr_accessible :name, :introduction
    end

    def check_active_requirements
    	errors.add(:active, "Questionário sem perguntas não pode ser ativado.") if self.active && self.questions.empty?
  	end
  end
end
